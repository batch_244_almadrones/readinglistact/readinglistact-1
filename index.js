function odd_or_even(num_1) {
  let remainder = num_1 % 2;
  let even_true = remainder == 0;
  return even_true;
}

let num_eval = prompt("Enter a number: ");
console.log(num_eval + " is even: " + odd_or_even(num_eval));
